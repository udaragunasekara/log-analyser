###### Log Analyser
This is a command line application that will analyse the given log file.

###### Usage 
- Build the jar file

```
./gradlew clean build
```

- Execute the jar file

```
java -jar build/libs/log-analyser-1.0-SNAPSHOT-all.jar -f src/test/resources/example.log

```

Help
```
java -jar build/libs/log-analyser-1.0-SNAPSHOT-all.jar --help

Display statistical information about logged requests to the STDOUT.
  -f, --file=<file>   the log file with the request logs
  -h, --help          Show this help message and exit.
  -V, --version       Print version information and exit.

```
###### Note:
The log file location is relative to the project folder

###### Requires
``
java 11
``