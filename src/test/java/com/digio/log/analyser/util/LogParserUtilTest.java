package com.digio.log.analyser.util;

import com.digio.log.analyser.model.LoggedRequest;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LogParserUtilTest {

    @Test
    void whenLogLineWithoutUser_then_parseLogLine() {
        String logLine = "177.71.128.21 - - [10/Jul/2018:22:21:28 +0200] \"GET /intranet-analytics/ HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (X11; U; Linux x86_64; fr-FR) AppleWebKit/534.7 (KHTML, like Gecko) Epiphany/2.30.6 Safari/534.7\"\n";
        LoggedRequest loggedRequest = LogParserUtil.parseLogLine(logLine);

        assertAll(() -> {
            assertNotNull(loggedRequest);
            assertEquals("177.71.128.21", loggedRequest.getIp());
            assertEquals("/intranet-analytics/", loggedRequest.getResource());
        });
    }

    @Test
    void whenLogLineWithOptionalUser_then_parseLogLine(){
        String logLineWithUser = "50.112.00.11 - admin [11/Jul/2018:17:33:01 +0200] \"GET /asset.css HTTP/1.1\" 200 3574 \"-\" \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6\"";
        LoggedRequest loggedRequest = LogParserUtil.parseLogLine(logLineWithUser);

        assertAll(() -> {
            assertNotNull(loggedRequest);
            assertEquals("50.112.00.11", loggedRequest.getIp());
            assertEquals("/asset.css", loggedRequest.getResource());
        });
    }

    @Test
    void whenValidLogFileIsGiven_then_parseLogFileAsStream() {
        String file = "src/test/resources/sample.log";
        Stream<LoggedRequest> stream = LogParserUtil.parseLogFile(file);
        assertNotNull(stream);
        assertEquals(4, stream.count());
    }

    @Test
    void whenFileWithInvalidLogLinesIsGiven_then_parseLogFileAsStreamAndIgnoreInvalidLines() {
        String file = "src/test/resources/sample-with-invalid-lines.log";
        Stream<LoggedRequest> stream = LogParserUtil.parseLogFile(file);
        assertNotNull(stream);
        assertEquals(3, stream.count());
    }
}