package com.digio.log.analyser.util;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CollectionsUtilTest {

    @Test
    void whenFrequencyMapWithDifferentKeysIsGiven_then_topXIsSortedFromFrequencyOnly() {
        Map<String, Integer> frequencyMap = Map.of("A", 1, "B", 2, "C", 3, "D", 4);
        List<String> top3 = CollectionsUtil.top(frequencyMap, 3);
        assertEquals(3,top3.size());
        assertArrayEquals(new String[]{"D","C","B"}, (String[])top3.toArray(new String[3]));
    }

    @Test
    void whenFrequencyMapWithEqualFrequencyKeysIsGiven_then_topXIsSortedFromKey() {
        Map<String, Integer> frequencyMap = Map.of("A", 2, "B", 2, "C", 3, "D", 4);
        List<String> top3 = CollectionsUtil.top(frequencyMap, 3);
        assertEquals(3,top3.size());
        assertArrayEquals(new String[]{"D","C","A"}, (String[])top3.toArray(new String[3]));
    }

    @Test
    void whenLimitIsGreaterThanMapIsGiven_then_FullSortedListIsReturned() {
        Map<String, Integer> frequencyMap = Map.of("A", 2, "B", 2, "C", 3, "D", 4);
        List<String> top3 = CollectionsUtil.top(frequencyMap, 10);
        assertEquals(4,top3.size());
        assertArrayEquals(new String[]{"D","C","A","B"}, (String[])top3.toArray(new String[3]));
    }
}