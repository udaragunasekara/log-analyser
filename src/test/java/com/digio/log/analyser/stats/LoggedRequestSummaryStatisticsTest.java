package com.digio.log.analyser.stats;

import com.digio.log.analyser.model.LoggedRequest;
import com.digio.log.analyser.util.LogParserUtil;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class LoggedRequestSummaryStatisticsTest {

    @Test
    void whenValidStreamOfLoggedRequestIsGiven_then_statisticsAreCalculated() {
        String file = "src/test/resources/sample.log";
        Stream<LoggedRequest> stream = LogParserUtil.parseLogFile(file);
        LoggedRequestSummaryStatistics statistics = stream.collect(new LoggedRequestStreamCollector());
        assertNotNull(statistics);
        assertEquals(3, statistics.getUniqueIpAddresses());
        assertArrayEquals(new String[]{"168.41.191.40", "168.41.191.41", "177.71.128.21"}, statistics.getTop3Ips().toArray(new String[3]));
        assertArrayEquals(new String[]{"/intranet-analytics/", "http://example.net/blog/category/meta/", "http://example.net/faq/"}, statistics.getTop3Urls().toArray(new String[3]));
    }

    @Test
    void testCombine() {
        var urls = new HashMap<>(Map.of("/x", new LoggedRequestSummaryStatistics.MutableInteger(2), "/y", new LoggedRequestSummaryStatistics.MutableInteger(4)));
        var ips = new HashMap<>(Map.of("1.2.3", new LoggedRequestSummaryStatistics.MutableInteger(3), "4.5.6", new LoggedRequestSummaryStatistics.MutableInteger(6)));

        var s1 = new LoggedRequestSummaryStatistics(urls, ips);
        var s2 = new LoggedRequestSummaryStatistics(urls, ips);

        s1.combine(s2);

        assertEquals(6, s1.getIpCounter().get("1.2.3").getCount());
        assertEquals(12, s1.getIpCounter().get("4.5.6").getCount());

        assertEquals(4, s1.getUrlCounter().get("/x").getCount());
        assertEquals(8, s1.getUrlCounter().get("/y").getCount());
    }
}