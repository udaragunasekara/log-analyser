package com.digio.log.analyser.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StreamingLogAnalyserServiceImplTest {

    private LogAnalyserService logAnalyserService = new StreamingLogAnalyserServiceImpl();

    @Test
    void whenALogFileIsGiven_then_fileIsAnalysed() {
        var file = "src/test/resources/example.log";
        var statistics = logAnalyserService.analyseLogfile(file);
        assertEquals(11, statistics.getUniqueIpAddresses());
        assertArrayEquals(new String[]{"168.41.191.40","177.71.128.21","50.112.00.11"}, statistics.getTop3Ips().toArray(new String[0]));
        assertArrayEquals(new String[]{"/docs/manage-websites/","/","/asset.css"}, statistics.getTop3Urls().toArray(new String[0]));
    }
}