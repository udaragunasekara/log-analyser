package com.digio.log.analyser.service;

import com.digio.log.analyser.model.LoggedRequest;
import com.digio.log.analyser.stats.LoggedRequestStreamCollector;
import com.digio.log.analyser.stats.LoggedRequestSummaryStatistics;
import com.digio.log.analyser.util.LogParserUtil;

import java.util.stream.Stream;

public class StreamingLogAnalyserServiceImpl implements LogAnalyserService {

    @Override
    public LoggedRequestSummaryStatistics analyseLogfile(String logFilePath) {
        try (Stream<LoggedRequest> stream = LogParserUtil.parseLogFile(logFilePath)){
            return stream.collect(new LoggedRequestStreamCollector());
        }
    }
}
