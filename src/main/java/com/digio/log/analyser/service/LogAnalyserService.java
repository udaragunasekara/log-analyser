package com.digio.log.analyser.service;

import com.digio.log.analyser.stats.LoggedRequestSummaryStatistics;

public interface LogAnalyserService {
    LoggedRequestSummaryStatistics analyseLogfile(String logFilePath);
}
