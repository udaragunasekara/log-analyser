package com.digio.log.analyser.model;

public enum RequestType {
    GET, POST, DELETE, PATCH
}
