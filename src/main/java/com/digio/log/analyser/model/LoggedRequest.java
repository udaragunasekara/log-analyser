package com.digio.log.analyser.model;

import lombok.Builder;
import lombok.Value;

/**
 * Represents a log line with request information
 */
@Value
@Builder
public class LoggedRequest {
    String ip;
    RequestType requestType;
    String resource;
    int responseCode;
    String browserInfo;
    String timeStamp;
    String user;
}
