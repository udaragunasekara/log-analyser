package com.digio.log.analyser.util;

import com.digio.log.analyser.model.LoggedRequest;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@UtilityClass
public class LogParserUtil {

    public static LoggedRequest parseLogLine(String logLine) {
        var parts = logLine.split("\"-\"");
        var requestInfoParts = parts[0].split(" ");
        var ip = requestInfoParts[0].trim();
        var resource = requestInfoParts[6];
        return LoggedRequest.builder()
                .ip(ip)
                .resource(resource)
                .build();
    }

    @SneakyThrows
    public static Stream<LoggedRequest> parseLogFile(String path) {
        return Files.lines(Path.of(path))
                .filter(LogParserUtil::isValidLogLine)
                .map(LogParserUtil::parseLogLine);
    }

    private static boolean isValidLogLine(String logLine){
        var parts = logLine.split("\"-\"");
        if(parts == null || parts.length != 2) return false;

        var requestInfoParts = parts[0].split(" ");
        if (requestInfoParts == null || requestInfoParts.length != 10) return false;

        return true;
    }
}
