package com.digio.log.analyser.util;

import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@UtilityClass
public class CollectionsUtil {

    /**
     * Gets the top n keys mapped in the frequency map.
     * Sorts the map by the frequency and by the key's natural order if the frequencies are equal
     *
     * @param frequencyMap
     * @param limit
     * @return top n keys defined by the limit
     */
    public static List<String> top(Map<String, Integer> frequencyMap, int limit) {
        return frequencyMap.entrySet()
                .stream()
                .sorted((e1, e2) -> e1.getValue() == e2.getValue() ? e1.getKey().compareTo(e2.getKey()) : e2.getValue().compareTo(e1.getValue()))
                .map(Map.Entry::getKey)
                .limit(limit)
                .collect(Collectors.toList());
    }
}
