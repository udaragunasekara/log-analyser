package com.digio.log.analyser.app;

import com.digio.log.analyser.service.LogAnalyserService;
import com.digio.log.analyser.service.StreamingLogAnalyserServiceImpl;
import com.digio.log.analyser.stats.LoggedRequestSummaryStatistics;
import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine;

import java.io.File;
import java.util.concurrent.Callable;

@Slf4j
@CommandLine.Command(name = "log-analyser", mixinStandardHelpOptions = true, version = "log-analyser 1.0",
        description = "Display statistical information about logged requests to the STDOUT.")
public class Application implements Callable<Integer> {

    @CommandLine.Option(names = {"-f", "--file"}, required = true, description = "the log file to analyse")
    File file;

    private LogAnalyserService analyserService = new StreamingLogAnalyserServiceImpl();

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Application()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
        stdout("Analysing file : " + file);
        stdout("----------------------------------");

        LoggedRequestSummaryStatistics statistics = analyserService.analyseLogfile(file.getAbsolutePath());
        log.info("finished analysing the file [{}] : {}", file, statistics);
        stdout("The number of unique IP addresses : " + statistics.getUniqueIpAddresses());
        stdout("The top 3 most visited URLs : " + statistics.getTop3Urls());
        stdout("The top 3 most active IP addresses : " + statistics.getTop3Ips());
        return null;
    }

    private void stdout(Object message) {
        System.out.println(message);
    }
}
