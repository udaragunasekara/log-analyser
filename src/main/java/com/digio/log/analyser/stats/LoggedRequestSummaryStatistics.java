package com.digio.log.analyser.stats;

import com.digio.log.analyser.model.LoggedRequest;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.digio.log.analyser.util.CollectionsUtil.top;

/**
 * A state object to collect statistics of LoggedRequests
 */
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class LoggedRequestSummaryStatistics implements LoggedRequestConsumer {

    @Getter(AccessLevel.PACKAGE)
    private Map<String, MutableInteger> urlCounter = new HashMap<>();
    @Getter(AccessLevel.PACKAGE)
    private Map<String, MutableInteger> ipCounter = new HashMap<>();

    @Override
    public void accept(LoggedRequest loggedRequest) {
        updateFrequency(loggedRequest.getResource(), urlCounter);
        updateFrequency(loggedRequest.getIp(), ipCounter);
    }

    public void combine(LoggedRequestSummaryStatistics other) {
        add(this.ipCounter, other.ipCounter);
        add(this.urlCounter, other.urlCounter);
    }

    public int getUniqueIpAddresses() {
        return ipCounter.size();
    }

    public List<String> getTop3Ips() {
        var map = mapToInt(ipCounter);
        log.debug("ip frequency map : {}", map);
        return top(map, 3);
    }

    public List<String> getTop3Urls() {
        var map = mapToInt(urlCounter);
        log.debug("url frequency map : {}", map);
        return top(map, 3);
    }

    private Map<String, Integer> mapToInt(Map<String, MutableInteger> counter) {
        var map = new HashMap<String, Integer>();
        counter.forEach((k, v) -> map.put(k, v.count));
        return map;
    }

    private void updateFrequency(String key, Map<String, MutableInteger> map) {
        map.merge(key, new MutableInteger(), (m, v) -> m.increment());
    }

    private void add(Map<String, MutableInteger> map1, Map<String, MutableInteger> map2) {
        map2.forEach((k, v) -> map1.merge(k, v, (oldValue, newValue) -> oldValue.add(newValue.count)));
    }

    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    static class MutableInteger {
        private int count = 1;

        public MutableInteger increment() {
            ++count;
            return this;
        }

        public MutableInteger add(int count) {
            this.count += count;
            return this;
        }
    }
}
