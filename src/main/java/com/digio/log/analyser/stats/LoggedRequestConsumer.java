package com.digio.log.analyser.stats;

import com.digio.log.analyser.model.LoggedRequest;

@FunctionalInterface
public interface LoggedRequestConsumer {
    void accept(LoggedRequest loggedRequest);
}
