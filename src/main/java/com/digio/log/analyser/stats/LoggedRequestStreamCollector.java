package com.digio.log.analyser.stats;

import com.digio.log.analyser.model.LoggedRequest;

import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

/**
 * {@link Collector} implementation for {@link LoggedRequest} to collect multiple statistics in one parse
 */
public class LoggedRequestStreamCollector implements Collector<LoggedRequest, LoggedRequestSummaryStatistics, LoggedRequestSummaryStatistics> {

    @Override
    public Supplier<LoggedRequestSummaryStatistics> supplier() {
        return LoggedRequestSummaryStatistics::new;
    }

    @Override
    public BiConsumer<LoggedRequestSummaryStatistics, LoggedRequest> accumulator() {
        return (a, t) -> a.accept(t);
    }

    @Override
    public BinaryOperator<LoggedRequestSummaryStatistics> combiner() {
        return (a1, a2) -> {
            a1.combine(a2);
            return a1;
        };
    }

    @Override
    public Function<LoggedRequestSummaryStatistics, LoggedRequestSummaryStatistics> finisher() {
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.CONCURRENT, Characteristics.IDENTITY_FINISH);
    }
}
